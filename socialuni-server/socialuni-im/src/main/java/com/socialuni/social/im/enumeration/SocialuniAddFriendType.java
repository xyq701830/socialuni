package com.socialuni.social.im.enumeration;

import com.socialuni.social.common.api.enumeration.ConstStatus;
import com.socialuni.social.common.api.enumeration.SocialuniCommonStatus;

import java.util.Arrays;
import java.util.List;

public class SocialuniAddFriendType {
    public static final String apply = ConstStatus.apply;
    public static final String accept = ConstStatus.accept;


    public static final List<String> values = Arrays.asList(SocialuniAddFriendType.apply, SocialuniAddFriendType.accept);


}
