export default class SocialuniRequestHeaderName {
    public static system = "system";
    public static platform = "platform";
    public static provider = "provider";
    public static socialuniCityAdCode = "@socialuni/socialuni-CityAdCode";
    public static socialuniCityLon = "@socialuni/socialuni-CityLon";
    public static socialuniCityLat = "@socialuni/socialuni-CityLat";
}
